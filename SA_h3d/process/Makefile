
#### Notes:
## Based on SA-alv @ notGeneralized
## Now using SA-alv as subtree like SA_µCT_Dragos


#### ToDo:


#### Done:
## do +/- scaling, iA ana
## k-density plot on interpl. angles (iA)
## extract field data of FA to CSV

SUBMODDIR?=../submodules/


SHELL:= /bin/bash -O extglob


### setting default paths of external programs
ITK?=$(SUBMODDIR)/ITK-CLIs/
PV?=/opt/paraview-5.3.0_GL1/
GP?=/opt/gnuplot-5.0.6/

export PATH:= $(ITK)/build:$(PATH)
export PATH:= $(PV)/bin:$(PATH)
export PATH:= $(GP)/bin:$(PATH)


### list internal programs
EXECUTABLES = resample subimage_extract distance_map_signed_maurer_f32 watershed_morph_f32_UI32 toUInt8 toUInt16 mask stat_SDI hist open_label-shape mask-negated analyse_labels file_converter close_bin_para open_bin_para thresh-glob max add_const
### also check uncommon tools
EXECUTABLES+= parallel


### check existance of external programs
K:= $(foreach exec,$(EXECUTABLES),\
	$(if $(shell PATH=$(PATH) which $(exec)),some string,$(error "No $(exec) in PATH")))


BASENAME = h3d
ROI= ROI100

##ALVs need to be present in *_Alv.mha
# ALVs= 29 39 40 42 49 50 53
# ALVsCSVs= $(ALVs:%=$(BASENAME)_seg_Alv_dmc_sws_o_a%_FA.csv)
# ALVsSVGs= $(ALVs:%=$(BASENAME)_seg_Alv_dmc_sws_o_a%_FA.svg)



SPACE := $(eval) $(eval)
base_ = $(subst $(SPACE),_,$(filter-out $(lastword $(subst _, ,$1)),$(subst _, ,$1)))
base. = $(subst $(SPACE),.,$(filter-out $(lastword $(subst ., ,$1)),$(subst ., ,$1)))


.PHONY : all clean


all : $(BASENAME)_seg_Alv.mha
all : SA/all


clean : SA/clean
	-rm -v !(Makefile|SA)


% :: %.gz # try decompress-first rule
	unpigz -v -- $<

%.gz : %
	pigz -v -- $< 



%_rsi+1.0.mha : %.mha
	resample $< $@ 1 0 1.0 1.0 1.0

%_ROI000.mha : %.mha
	ln -sf $< $@

%_ROI100.mha : %.mha
	subimage_extract $< $@ 0  520  680  93  2000 1700 940

%_ROI800.mha : %.mha
	subimage_extract $< $@ 0  518  873  93  1700 1700 940

%_ROI900.mha : %.mha
	subimage_extract $< $@ 0  661 1118 263  1000 1000 770

%_dmsm+0+0.mha : %.mha
	distance_map_signed_maurer_f32 $< $@ 1 0 0

%_wsm+02+1+0.mha : %.mha
	watershed_morph_f32_UI32 $< $@ 1 02 1 0 0 # compression OK
	stat_SDI $@ 1 # (potential name clash with stat from Linux)
	toUInt16 $@ $@ 1 # essential or hist might have trouble

%_wsm+05+1+0.mha : %.mha
	watershed_morph_f32_UI32 $< $@ 1 05 1 0 0 # compression OK
	stat_SDI $@ 1 # (potential name clash with stat from Linux)
	toUInt16 $@ $@ 1 # essential or hist might have trouble

%_noBL.mha : %.mha
	open_label-shape $< $@ 0 0 0 1 NumberOfPixelsOnBorder

%_minS.mha : %.mha
	open_label-shape $< $@ 0 0 512 0 NumberOfPixels # FA needs ~ 8^3 voxels 

%-Bt.mha : %.mha $(BASENAME)_seg_Bt.mha
	mask-negated $^ $@ 0

%_th+0.mha : %.mha
	thresh-glob $< $@  0  0 0
	toUInt8 $@ $@ 0

%+1.mha : %.mha
	add_const $< $@ 0  1

%_th@1.mha : %.mha
	thresh-glob $< $@  0  1 max 1
	toUInt8 $@ $@ 0

$(BASENAME)_seg_aAlv.mha : $(BASENAME)_seg_a+AW_dmsm+0+0_wsm+02+1+0.mha $(BASENAME)_seg_a-Bt_$(ROI)_rsi+1.0.mha # mask wsw with a-Bt
	mask $^ $@ 1

$(BASENAME)_seg_aDA.mha : $(BASENAME)_seg_a-Bt_$(ROI)_rsi+1.0_+35+0obp.mha
	ln -sf $< $@

$(BASENAME)_seg_a+AW.mha : $(BASENAME)_seg_a-Bt_$(ROI)_rsi+1.0.mha $(BASENAME)_seg_aDA_th+0.mha # remove duct. air spaces (DA)
	mask $^ $@ 0 

$(BASENAME)_seg_aAlvAW.mha : $(BASENAME)_seg_aAlv_noBL+1.mha $(BASENAME)_seg_a+AW.mha # noBL before labels+1 # remove labels where DA should go
	mask $^ $@ 0

$(BASENAME)_seg_Alv.mha : $(BASENAME)_seg_aAlvAW_minS.mha $(BASENAME)_seg_aDA_th@1.mha # add DA@1
	max $^ $@ 0

%.mhd : %.mha
	file_converter $< $@ 0
	awk -i inplace '{print ($$1 == "ElementSpacing") ? sprintf("ElementSpacing = %f %s %s", $$3*1e-3, $$4*1e-3, $$5*1e-3) : $$0}' $@


SA/$(BASENAME)_seg_Alv.mha : $(BASENAME)_seg_Alv.mhd
	stat_SDI $< 1
	toUInt16 $< $@ 0 # hist segfaults for UINT

SA/all : SA/$(BASENAME)_seg_Alv.mha
SA/% :
	$(MAKE) -C $(dir $@) $*  SUBMODDIR=../$(SUBMODDIR) BASENAME=$(notdir $(basename $<)) ELLERR=0.015 PJOBS=30


#prevent removal of any intermediate files
.SECONDARY: 

.SECONDEXPANSION:

%+0cbp.mha : $$(call base_,%).mha
	$(eval v= $(subst $(basename $<),,$*))
	$(eval v= $(subst _,,$(v)))
	$(eval v= $(subst +0cbp,,$(v)))
	close_bin_para $< $@ 0 $(v) 0

%+0obp.mha : $$(call base_,%).mha
	$(eval v= $(subst $(basename $<),,$*))
	$(eval v= $(subst _,,$(v)))
	$(eval v= $(subst +0cbp,,$(v)))
	open_bin_para $< $@ 0 $(v) 0
