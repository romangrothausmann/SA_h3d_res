
#### Notes:
## using (3D) FacetAnalizer to do Facet Analyses (FA)


#### ToDo:


#### Done:
## do +/- scaling, iA ana
## k-density plot on interpl. angles (iA)
## extract field data of FA to CSV


AU?=10.0
ELLERR?=10.0
PJOBS?=12

SUBMODDIR?=../submodules/


SHELL:= /bin/bash -O extglob


### setting default paths of external programs
ITK?=$(SUBMODDIR)/ITK-CLIs/
VTK?=$(SUBMODDIR)/VTK-CLIs/
FA?=$(SUBMODDIR)/FacetAnalyser/
PV?=/opt/paraview-5.0.1_GL2
GP?=/opt/gnuplot-5.0.3/

export VGLRUN?=vglrun

export PATH:= $(ITK)/build:$(PATH)
export PATH:= $(VTK)/build:$(PATH)
export PATH:= $(FA)/build:$(PATH)
export PATH:= $(PV)/bin:$(PATH)
export PATH:= $(GP)/bin:$(PATH)


### list internal programs
EXECUTABLES = hist analyse_labels file_converter
EXECUTABLES+= mc_discrete smooth-ws threshold vtp2csv vtpFD2csv tf_scale analyse_S+V
EXECUTABLES+= FacetAnalyserCLI
### list external programs
EXECUTABLES+= paraview pvpython
EXECUTABLES+= gnuplot
EXECUTABLES+= octave-cli
EXECUTABLES+= $(VGLRUN)
### also check uncommon tools
EXECUTABLES+= datamash pigz parallel
## inkscape (without PDF-export bug, e.g. 0.48.5) and inkscape > 0.91
EXECUTABLES+= inkscape inkscape-0.92
EXECUTABLES+= convert mogrify


### check existance of external programs
K:= $(foreach exec,$(EXECUTABLES),\
	$(if $(shell PATH=$(PATH) which $(exec)),some string,$(error "No $(exec) in PATH")))


## run IM tools without setting cration and modification date (or -strip) to avoid changes in PNGs that are not visual: http://stackoverflow.com/questions/13577280/getting-imagemagick-convert-to-not-write-out-extra-info
## some bug now needs even more default options: http://unix.stackexchange.com/questions/255252/create-the-same-png-with-imagemagick-2-times-binaries-differ#255256
convertIM:= convert -define png:exclude-chunks=date,time +set date:create +set date:modify
mogrifyIM:= mogrify -define png:exclude-chunks=date,time +set date:create +set date:modify



BASENAME?= LabelIMG
## angle uncertainty for FA and GP, (FA int. scales to rad: 1.0/2/sin($(AU)/180.0*GPVAL_pi)**2), GP in deg therefore AU as is)
## using FWHM_GP = SIGMA_FA: sigma=$(AU)/2/sqrt(2*log(2)) for AU=10 FWHM_GP~4.247

##ALVs need to be present in *_Alv.mha
# ALVs= 29 39 40 42 49 50 53
# ALVsCSVs= $(ALVs:%=$(BASENAME)_dmc_sws_o_a%_FA.csv)
# ALVsSVGs= $(ALVs:%=$(BASENAME)_dmc_sws_o_a%_FA.svg)

aALL_CSVs= \
$(BASENAME)_dmc_sws_o_aALL_FA.csv \
$(BASENAME)_dmc_sws_o_aALL_FA-edges.csv \
$(BASENAME)_dmc_sws_o_sX+0.5_aALL_FA.csv \
$(BASENAME)_dmc_sws_o_sX+0.5_aALL_FA-edges.csv \
$(BASENAME)_dmc_sws_o_sX+2.0_aALL_FA.csv \
$(BASENAME)_dmc_sws_o_sX+2.0_aALL_FA-edges.csv
aALL_SVGs= $(aALL_CSVs:%.csv=%.svg)

OVbase = $(BASENAME)_dmc_sws_o
## scaled versions:
# OVbase+= $(BASENAME)_dmc_sws_o_sX+0.5 $(BASENAME)_dmc_sws_o_sX+2.0
OVpdf = $(OVbase:%=%_FA-ov.pdf)
OVSpdf = $(OVbase:%=%_FA-ovS.pdf)

SPACE := $(eval) $(eval)
base_ = $(subst $(SPACE),_,$(filter-out $(lastword $(subst _, ,$1)),$(subst _, ,$1)))
base. = $(subst $(SPACE),.,$(filter-out $(lastword $(subst ., ,$1)),$(subst ., ,$1)))
noO = $(subst _o_,_,$1)
noF = $(subst _FA,, $1)
noOF = $(subst _FA,,$(call noO, $1))


.PHONY : all clean


all : $(BASENAME).mha
all : FA.done


clean :
	-rm -v !(Makefile|FA_base.pvsm|FA_fig_base.tex|FA_fig-tFA_base.tex|FA_ov_base.tex|OV_01.pvsm|MF4superProject|ellType.json|fail.svg)

FA.done : $(OVpdf) # do not use . within file name (e.g. .tFA) because latex will complain about unknown extension!
FA.done : $(OVSpdf)
# FA.done : $(ALVsSVGs)
FA.done:
	touch $@


% :: %.gz # try decompress-first rule
	unpigz -v -- $<

%.gz : %
	pigz -v -- $< 




%.mhd : %.mha
	file_converter $< $@ 0

%_sX+0.5.mhd : %.mhd
	awk '{print ($$1 == "ElementSpacing") ? sprintf("ElementSpacing = %f %s %s", $$3*0.5, $$4, $$5) : $$0}' $< > $@

%_sX+2.0.mhd : %.mhd
	awk '{print ($$1 == "ElementSpacing") ? sprintf("ElementSpacing = %f %s %s", $$3*2.0, $$4, $$5) : $$0}' $< > $@

%.ana : %.mhd
	analyse_labels $< 1 > $@
	awk -i inplace 'BEGIN{pi = atan2(0, -1)} NR==1 {printf("%s\tDv\n", $$0)} NR>1 {printf("%s\t%f\n", $$0, 2*(3/4/pi*$$24)^(1/3))}' $@ # add column Dv, could come from analyse_labels GetEquivalentSphericalRadius()

%.anaGP : %.ana # gnuplot ignores all #-lines, even headers!
	sed '1s/#//;2d' $< > $@ # also remove label 1 (2d)

%.efit : %.ana # ell_eval expects less columns
	awk '{print $$1,$$8,$$9,$$10,$$11,$$12,$$13,$$14,$$15,$$16,$$17,$$18,$$19,$$20,$$21,$$22,$$24}'  $< > $@

### BEGIN as in $(SUBMODDIR)/ell_eval/test/Makefile

%.efit.ells \
%.efit-3Dsym.svg \
%.efit-3Dasym.svg \
%.efit-2Dpdist.svg \
%.efit-2Dhist.svg \
%.efit-2Danno.svg \
%.efit-2DpdistOV.svg \
%.efit-2Dshist.svg \
 : %.efit
	octave-cli $(SUBMODDIR)/ell_eval/ell_type.m $< $(ELLERR)

include $(SUBMODDIR)/ell_eval/Makefile

### END as in $(SUBMODDIR)/ell_eval/test/Makefile

# ### joining prereq. defined by multiple pattern rules seems not to work for time-stamp dependencies as it does for static rules
# ### the following rules without recipes will define a kind of "order-only" prerequisites! (bug?)
# $(OVbase:%=%_tET.ells) : $(BASENAME)_dmc_sws_o%_tET.ells : $(BASENAME)%.efit.ells
# $(OVbase:%=%_tET3D.svg) : $(BASENAME)_dmc_sws_o%_tET3D.svg : $(BASENAME)%.efit-3Dasym_VB.svg
# $(OVbase:%=%_tET.svg) : $(BASENAME)_dmc_sws_o%_tET.svg : $(BASENAME)%.efit-2Dpdist_VB.svg
# $(OVbase:%=%_tET3D.svg) $(OVbase:%=%_tET.svg) $(OVbase:%=%_tET.ells) :
# 	ln -sf $< $@

### to get expected time-stamp dependence, repeate recipe or use macros: http://stackoverflow.com/questions/27836246/gnu-makefile-pattern-rule-with-multiple-targets-with-one-dependency-ignores-al#comment44116678_27839981

$(OVbase:%=%_tET.ells) : $(BASENAME)_dmc_sws_o%_tET.ells : $(BASENAME)%.efit.ells
	ln -sf $< $@
$(OVbase:%=%_tET3D.svg) : $(BASENAME)_dmc_sws_o%_tET3D.svg : $(BASENAME)%.efit-3Dasym_VB.svg
	ln -sf $< $@
$(OVbase:%=%_tET.svg) : $(BASENAME)_dmc_sws_o%_tET.svg : $(BASENAME)%.efit-2Dpdist_VB.svg
	ln -sf $< $@
$(OVbase:%=%_tETov.svg) : $(BASENAME)_dmc_sws_o%_tETov.svg : $(BASENAME)%.efit-2Dshist_inc-pdist-shearY_VB.svg
	ln -sf $< $@
	inkscape-0.92 -z --file=$@ --export-pdf=$(basename $@).pdf --export-latex --export-area-page  && sed -i 's/$(basename $@).pdf/{$(basename $@)}.pdf/g' $(basename $@).pdf_tex # && sed -i '/^.*page=[0-9]*[02-9].*$$/d' $(basename $@).pdf_tex # <use ...> needs inkscape > 0.48.5 but upto 0.92pre1 buggy export to PDF+TEX (for hist-plots but not the tETov-SVG) # 0.92pre1 seems to create multi-page PDFs and expects one page more in the pdf_tex: http://tex.stackexchange.com/questions/243499/sharelatex-pdf-inclusion-required-page-does-not-exist-5#268880

%_dmc.vtp %_dmc.lst : %.mha Alv.lst
	mc_discrete $< $*_dmc.vtp 1 `head -n1 $(word 2,$^)` `tail -n1 $(word 2,$^)` 0 > $*_dmc.lst

%_sws.vtp : %.vtp
	smooth-ws $< $@ 0 120 1 1 # hardly any changes visible between 120 and 200 iterations

%_o.vtp : %.vtp
	threshold  $< $@  0 0 Scalars_ 0 0 0 # adjecency is stored as point data, AllScalars: 1 leads to gaps, 0 to "scars"

%_sX+0.5.vtp : %.vtp # possibly better off in .SECONDEXPANSION:
	tf_scale $< $@ 0  0.5 1 1

%_sX+2.0.vtp : %.vtp # possibly better off in .SECONDEXPANSION:
	tf_scale $< $@ 0  2.0 1 1

%_FA.vtp %_FA-hull.vtp %_FA-edges.vtp : %.vtp
	FacetAnalyserCLI $< 50 $(AU) 0.2 0.001 10 $*_FA.vtp $*_FA-hull.vtp $*_FA-edges.vtp # do not use $@ as this varies dep. on what Make demands first

%_FA-edges.csv : %_FA-edges.vtp
	vtp2csv $< $@ 2
# can happen if hull creation fails, only important for *_FA.csv: 	@TEST=` awk -F, '{if(!$$2) print FILENAME, NR, $$0}' $@ ` ; `# @ essential, or error will be printed anyway` ; f\
		[[ -n $$TEST ]] && { echo "ERROR interplanarAngle missing: _$${TEST}_" ; exit 1 ; } || true # abort if rows are missing interplanarAngle
#	awk -i inplace -F, '{if($$2) print $$0}' $@ # remove rows missing interplanarAngle, or gnuplot will assume multiple datasets # http://stackoverflow.com/questions/16529716/awk-save-modifications-inplace

%_FA.csv : %_FA.vtp
	vtpFD2csv $< | datamash -t, --no-strict --filler="" transpose > $@
	@TEST=` awk -F, '{if(!$$11) print FILENAME, NR, $$0}' $@ ` ; `# @ essential, or error will be printed anyway` ; \
		[[ -n $$TEST ]] && { echo "ERROR interplanarAngle missing: _$${TEST}_" ; exit 1 ; } || true # abort if rows are missing interplanarAngle
#	awk -i inplace -F, '{if($$11) print $$0}' $@ # remove rows missing interplanarAngle, or gnuplot will assume multiple datasets # http://stackoverflow.com/questions/16529716/awk-save-modifications-inplace

# $(BASENAME)_dmc_sws_o_aALL_FA.csv : $(ALVsCSVs)
# $(BASENAME)_dmc_sws_o_aALL_FA-edges.csv : $(ALVsCSVs:%_FA.csv=%_FA-edges.csv)
# $(BASENAME)_dmc_sws_o_sX+0.5_aALL_FA.csv : $(subst _o_,_o_sX+0.5_,$(ALVsCSVs))
# $(BASENAME)_dmc_sws_o_sX+0.5_aALL_FA-edges.csv : $(subst _o_,_o_sX+0.5_,$(ALVsCSVs:%_FA.csv=%_FA-edges.csv))
# $(BASENAME)_dmc_sws_o_sX+2.0_aALL_FA.csv : $(subst _o_,_o_sX+2.0_,$(ALVsCSVs))
# $(BASENAME)_dmc_sws_o_sX+2.0_aALL_FA-edges.csv : $(subst _o_,_o_sX+2.0_,$(ALVsCSVs:%_FA.csv=%_FA-edges.csv))

# $(aALL_CSVs) :
# 	cat $^ | sed '2,$${/^[^-0-9,].*$$/d}' > $@ # ',' after 9 essential to keep empty cells; first '-' essential to keep lines starting with negative numbers! # unix.stackexchange.com/questions/76105/how-to-sed-a-range-of-lines#76106

Alv.lst : $(BASENAME).mha
	hist $< | awk 'NR>9 { if($$2){ printf("%06d\n", $$1)} }' | sed -r /^0+$$/d > $@ # NR>1 removes header, NR>9 ingores label 0 to 7 # hist segfaults for UINT => base-file should be USHORT
	wc -l $@

%_faN.tsv %_tFA.csv %_tFAe.csv %_ell.eta %_tot.vtpana : Alv.lst %.vtp $(BASENAME).neiN # %.vtp $(BASENAME).neiN to avoid re-exec. afterwards
	-rm $*.err
	-parallel --retries 3 --eta -j$(PJOBS) -u "make -j1 $*_a{}_FA.tex &>> $*.err" < $< # ignore exit status because failures before retry are counted # retries: rendering somtimes fails without obvious reason
#	parallel --halt 2 --eta -j$(PJOBS) -u "make -j1 $*_a{}_FA.tex &>> $*.err" < $< # rerun parallel (not ignoring exit status) to check retries were finally successful
#	! egrep -i 'fail|error' $*.err # currently ignored due to temp. segfault of FA
	! egrep -i 'ERROR interplanarAngle missing' $*.err # ! because ret val needs inversion (bash function, not from make, therefore a space is needed)
	while read i; do FN=$*_a$${i}_FA.csv; if [ -f $$FN ]; then awk -F, -v l=$$i '$$7 ~ /^[[:digit:]]/{n++} END{printf("%d\t%d\n", l, n)}' $$FN ; fi ; done < $<   > $*_faN.tsv # awk has no END for each file so using bash instead
	cat $*_a+([0-9])_FA.csv | sed '2,$${/[a-z]/d}' > $*_tFA.csv # not a UUOC! # remove all headers EXCEPT the first https://unix.stackexchange.com/questions/76105/how-to-delete-blank-lines-starting-from-line-5#76106
	cat $*_a+([0-9])_FA-edges.csv | sed '2,$${/[a-z]/d;/^$$/d;/^$$/d}' > $*_tFAe.csv # again, also remove empty lines (from failed hulls) to avoid GP interpretation as new dataset
	cat $*_a+([0-9]).vtpana | sed '1s/^#//;2,$${/^#/d}' > $*_tot.vtpana # remove # before header for GP and all other headers # expecting a*.vtpana build for *_FA.tex
	awk '/\\eta/{print $$3}' $*_a*.tsv > $*_ell.eta # expecting *.tsv build for *_FA.tex

# for parentheses inside a make-function: https://stackoverflow.com/a/8079904
rparen:=)
%.svg : %.csv fail.svg # dep. on fail.svg will cause wrong dep-branch if fail.svg does not exist!
	$(eval IPA= $(shell case $* in  *-edges|*_tFAe$(rparen) echo "\\\\\\theta\_e" ;; *$(rparen) echo "\\\\\\theta\_a" ;; esac)) # https://stackoverflow.com/questions/22003826/setting-a-variable-in-a-one-liner-bash-script 
	gnuplot -e "datafile='$<'; outfile='$@'; bin=$(AU); sigma=bin; xmin=0; xmax=180; col='interplanarAngles'; sep=','; xlabel='interplanar angle \$$$(IPA)$$ [$$\\^\\\\circ$$]';" $(SUBMODDIR)/gnuplot/hist-ar+kd.gp \
	|| ln -sf $(word 2,$^) $@ # link to fail.svg in case GP fails (due to missing data points if hull creation failed)
	@test -f $@ # gnuplot' exit dose not allow specifying a return value
	inkscape -z --file=$@ --export-pdf=$*.pdf --export-latex --export-area-page && sed -i 's/$*.pdf/{$*}.pdf/g' $*.pdf_tex

%_Vv.svg : %.anaGP
	gnuplot -e "datafile='$<'; outfile='$@'; bin=1000; sigma=bin; col='voxel'; xlabel='# of voxel'" $(SUBMODDIR)/gnuplot/hist-ar+kd.gp # voxel = $23
	@test -f $@ # gnuplot' exit dose not allow specifying a return value

%_Vphy.svg : %.anaGP
	gnuplot -e "datafile='$<'; outfile='$@'; bin=2e-5; sigma=bin; xmin=0; xmax=0.001; col='Vphy'; xlabel='physical volume \$$V\$$ [mm$$\^3$$]'" $(SUBMODDIR)/gnuplot/hist-ar+kd.gp # Vphy = $24 # $ needs to be escaped here for enhanced term
	@test -f $@ # gnuplot' exit dose not allow specifying a return value

%_Aphy.svg : %.anaGP
	gnuplot -e "datafile='$<'; outfile='$@'; bin=2e-3; sigma=bin; xmin=0; xmax=0.1; col='Aphy'; xlabel='physical surface (closed) \$$S\_c\$$ [mm$$\^2$$]'" $(SUBMODDIR)/gnuplot/hist-ar+kd.gp # Aphy = $25 # $ needs to be escaped here for enhanced term
	@test -f $@ # gnuplot' exit dose not allow specifying a return value

%_Dv.svg : %.anaGP
	gnuplot -e "datafile='$<'; outfile='$@'; bin=0.005; sigma=bin; xmin=0; xmax=0.2; col='Dv'; xlabel='diameter from physical volume \$$d\_v\ = \\\\sqrt\[3\]\{3 / 4 / \\\\pi * V\}$$ [mm]'" $(SUBMODDIR)/gnuplot/hist-ar+kd.gp # Dv = $26 # $ needs to be escaped here for enhanced term
	@test -f $@ # gnuplot' exit dose not allow specifying a return value

%_So.svg : %_tot.vtpana
	gnuplot -e "datafile='$<'; outfile='$@'; bin=2e-3; sigma=bin; xmin=0; xmax=0.1; col='S'; xlabel='physical surface (opened) \$$S\_o\$$ [mm$$\^2$$]'" $(SUBMODDIR)/gnuplot/hist-ar+kd.gp # S = $3 # $ needs to be escaped here for enhanced term
	@test -f $@ # gnuplot' exit dose not allow specifying a return value

%_SlSo.svg : %_SlSo.ana
	gnuplot -e "datafile='$<'; outfile='$@'; bin=0.05; sigma=bin; xmin=0; xmax=2; col='SlSo'; xlabel='ratio of alv. lit to \$$S\_o\$$: \$$S\_l\/S\_o = (S\_c\ - S\_o)/S\_o \$$ []'" $(SUBMODDIR)/gnuplot/hist-ar+kd.gp # S = $3 # $ needs to be escaped here for enhanced term
	@test -f $@ # gnuplot' exit dose not allow specifying a return value

%_SlSo.ana : $(BASENAME).ana %_tot.vtpana
	paste <( sed '2d' $< ) $(word 2,$^)  > $@ # remove label 1, which is not regarded in %_tot.vtpana 
	awk -i inplace 'NR==1 {printf("%s\tSlSo\n", $$0)} NR>1 {printf("%s\t%f\n", $$0, $$25 / $$29 - 1)}' $@ # add column SlSo
	sed -i '1s/#//' $@

%.calc : %.ana # using Vphy = $24, Aphy = $25; or use: https://unix.stackexchange.com/a/25144
	awk ' \
	    BEGIN {print "i g Cp Psi"} \
	    NR>1 { \
	           pi = atan2(0, -1); \
	           g= 36 * pi * $$24^2 / $$25^3; \
	           c= g^(1/2); \
	           p= g^(1/3); \
	           print $$1, g, c, p \
	         }' $< > $@

%.vtpana : %.vtp
	analyse_S+V $< > $@

%.pvsm : %.nei
	pvpython $(SUBMODDIR)/paraview/scripts/pvsm-multi.py -o $@ -i \
	` while read i; do printf "D01_Alv_dmc_sws_o_a%06d_FA.pvsm " $$i ; done < $< ` # could put a make *.pvsm in to create dep. files

%_L2.nei : %.mha # should go in secondexpansion
	octave-cli $(SUBMODDIR)/octave/scripts/neighbours3D.m $< 2 \
	| awk 'NR>1 && $$2>9 {print $$2}' > $@ # exclude header and first 9 labels

%.nei : %.mha
	octave-cli $(SUBMODDIR)/octave/scripts/neighbours3D.m $< > $@

%.neiN : %.nei Alv.lst
	awk 'NR>1 && !($$2==1) {printf("%06d\n", $$1)}' $< `# adj. logic in () to excude further labels; %06d needs to match Alv.lst` \
	| tee temp.nei `# create copy for det. of labels with no neighbour` \
	| sort -n `# uniq needs sorted input!` \
	| uniq -c > $@ # uniq -c puts counts in first col.
	grep -vFf temp.nei $(word 2,$^) | awk '{print 0, $$0}' >> $@ # list lines that are not in temp.nei https://stackoverflow.com/questions/3724786/how-to-diff-two-file-lists-and-ignoring-place-in-list#3725242
	-rm temp.nei

%_neiN.svg : %.neiN
	gnuplot -e "datafile='$<'; outfile='$@'; bin=1; sigma=bin; xmin=0; xmax=25; col=1; xlabel='\\\\# neighbours $$\\\\Xi$$'" $(SUBMODDIR)/gnuplot/hist-ar+kd.gp # uniq -c puts counts in first col.
	@test -f $@ # gnuplot' exit dose not allow specifying a return value

%_faN.svg : %_faN.tsv
	gnuplot -e "datafile='$<'; outfile='$@'; bin=1; sigma=bin; xmin=0; xmax=25; col=2; xlabel='\\\\# facets $$\\\\Upsilon$$'" $(SUBMODDIR)/gnuplot/hist-ar+kd.gp # uniq -c puts counts in first col.
	@test -f $@ # gnuplot' exit dose not allow specifying a return value

%_g.svg : %.calc
	gnuplot -e "datafile='$<'; outfile='$@'; bin=0.02; sigma=bin; xmin=0; xmax=1; col='g'; xlabel='$$\\\\gamma = 36 \\\\pi V^2/S^3$$'" $(SUBMODDIR)/gnuplot/hist-ar+kd.gp
	@test -f $@ # gnuplot' exit dose not allow specifying a return value

%_Cp.svg : %.calc
	gnuplot -e "datafile='$<'; outfile='$@'; bin=0.02; sigma=bin; xmin=0; xmax=1; col='Cp'; xlabel='sphericity $$c_p = \\\\sqrt\{\\\\gamma\}$$'" $(SUBMODDIR)/gnuplot/hist-ar+kd.gp
	@test -f $@ # gnuplot' exit dose not allow specifying a return value

%_Psi.svg : %.calc
	gnuplot -e "datafile='$<'; outfile='$@'; bin=0.02; sigma=bin; xmin=0; xmax=1; col='Psi'; xlabel='sphericity $$\\\\Psi = S\_s/S\_p = \\\\sqrt\[3\]\{\\\\gamma\}$$'" $(SUBMODDIR)/gnuplot/hist-ar+kd.gp
	@test -f $@ # gnuplot' exit dose not allow specifying a return value

%_Eta.svg : %_ell.eta
	gnuplot -e "datafile='$<'; outfile='$@'; bin=0.02; sigma=bin; xmin=0; xmax=1; col=1; xlabel='ellipsoidity $$\\\\eta = S\_e/S\_p$$'" $(SUBMODDIR)/gnuplot/hist-ar+kd.gp # escape underscore for enhanced term https://stackoverflow.com/a/32860899
	@test -f $@ # gnuplot' exit dose not allow specifying a return value

$(OVbase:%=%_tVphy.svg) : $(BASENAME)_dmc_sws_o%_tVphy.svg : $(BASENAME)%_Vphy.svg
	ln -sf $< $@

$(OVbase:%=%_tAphy.svg) : $(BASENAME)_dmc_sws_o%_tAphy.svg : $(BASENAME)%_Aphy.svg
	ln -sf $< $@

$(OVbase:%=%_Dv.svg) : $(BASENAME)_dmc_sws_o%_Dv.svg : $(BASENAME)%_Dv.svg
	ln -sf $< $@

$(OVbase:%=%_tg.svg) : $(BASENAME)_dmc_sws_o%_tg.svg : $(BASENAME)%_g.svg
	ln -sf $< $@

$(OVbase:%=%_tCp.svg) : $(BASENAME)_dmc_sws_o%_tCp.svg : $(BASENAME)%_Cp.svg
	ln -sf $< $@

$(OVbase:%=%_tPsi.svg) : $(BASENAME)_dmc_sws_o%_tPsi.svg : $(BASENAME)%_Psi.svg
	ln -sf $< $@

$(OVbase:%=%_neiN.svg) : $(BASENAME)_dmc_sws_o%_neiN.svg : $(BASENAME)%_neiN.svg
	ln -sf $< $@

%.png : %.pvsm %.vtp
	$(VGLRUN) pvpython $(SUBMODDIR)/paraview/scripts/render-view.py -s 600 600 -r -i $< -png $@.png
	$(convertIM) $@.png -trim +repage \( +clone -rotate 90 +clone -mosaic +level-colors white \) +swap -gravity center -composite  $@ # square image after autocrop http://www.imagemagick.org/Usage/thumbnails/#square # autocrop, PV auto view can yield large borders https://www.imagemagick.org/discourse-server/viewtopic.php?t=15675#p55770
	rm $@.png # mogrify has no -clone https://www.imagemagick.org/discourse-server/viewtopic.php?t=14576

%_FA.tex : FA_fig_base.tex %.tsv %_FA.svg %_FA-edges.svg %_FA.png %.vtpana
	sed 's/FA-base/$*_FA/g' $< > $@
	sed -i 's/base-table.tsv/$*.tsv/g' $@

%_tFA.tex : FA_fig-tFA_base.tex %_tVphy.svg %_Dv.svg %_So.svg %_SlSo.svg %_tPsi.svg %_Eta.svg %_tFA.svg %_tFAe.svg %_neiN.svg %_faN.svg %_tET3D.svg %_tET.svg %_tETov.svg %_tET.ells # _tFA and _tFAe hardcoded in FA_fig-tFA_base.tex
	sed 's/FA-base/$*/g' $< > $@
	sed -i '/%% MARKER-LINE for including last line of external efit.ells:/r'<( tail -n1 $(lastword $^) | sed 's/#*//g;s/ [^ ]*$$/ $(ELLERR) [mm]/' ) $@

%_FA-ov.tex : FA_ov_base.tex %_tFA.tex Alv.lst
	sed 's/FA-base/$*/g' $< > $@
# no fail Message:	sed "/%% MARKER-LINE for including external idv-Figs:/r"<( ls -1 $*_a*_FA.tex | sed 's/.tex$$//g' | awk '{printf("\\depinc{%s}\n", $$1)}' ) $< > $@ # http://unix.stackexchange.com/questions/32908/how-to-insert-the-content-of-a-file-into-another-file-before-a-pattern-marker#32912
	sed -i "/%% MARKER-LINE for including external idv-Figs:/r"<( awk '{printf("\\depinc{$*_a%s_FA}\n", $$1)}' $(lastword $^) ) $@ # http://unix.stackexchange.com/questions/32908/how-to-insert-the-content-of-a-file-into-another-file-before-a-pattern-marker#32912
	sed -i '/%% MARKER-LINE for including external tFA-Figs:/a\\\\depinc{$*_tFA.tex}' $@ # http://stackoverflow.com/questions/17394402/insert-a-double-backslash-using-sed

%_FA-ovS.tex : FA_ov_base.tex %_tFA.tex
	sed 's/FA-base/$*/g' $< > $@
	sed -i '/%% MARKER-LINE for including external tFA-Figs:/a\\\\depinc{$*_tFA.tex}' $@ # http://stackoverflow.com/questions/17394402/insert-a-double-backslash-using-sed

%.pdf : %.tex
	pdflatex -interaction=nonstopmode -shell-escape $<


pv-% : %.pvsm
	$(VGLRUN) paraview --state=$<

$(BASENAME)_l%.nlst : $(BASENAME)_l%.mha
	octave-cli $(SUBMODDIR)/octave/scripts/neighbours3D.m $< $* \
	| awk 'if($$2 > 3){print $$2}' > $@

$(BASENAME)_l%.pvsm : $(BASENAME)_l%.nlst FA.done
	$(VGLRUN) pvpython $(SUBMODDIR)/paraview/scripts/pvsm-multi.py -o $@ -i `awk '{printf("$(BASENAME)_dmc_sws_o_a%s_FA.pvsm ", $0)'`

#prevent removal of any intermediate files
.SECONDARY: 

.SECONDEXPANSION:

%.vtp : $$(call base_,%).vtp
	@echo "process $+ to $*"
	$(eval pos= $(subst $(basename $+),,$*))
	$(eval pos= $(subst _a,,$(pos)))
	threshold  $< $@  0 1 Scalars_ $(pos) $(pos) 1 # label is stored as cell data, AllScalars does not matter for cell data

%_ell.vtp : $$(call base_,%)_tET.ells
	@echo "process $+ to $*"
	$(eval pos= $(subst $(subst _tET.ells,,$+),,$*))
	$(eval pos= $(subst _a,,$(pos)))
	awk 'BEGIN{ln="$(pos)"; gsub ("^0*", "", ln)} {if($$17 == ln) print $$0}' $< \
	    | genEll $@ 0  0.5 100 100 1

%.pvsm : FA_base.pvsm %.vtp $$(call noOF,%).vtp $$(call noF,%)_ell.vtp
	@echo "process $^"
	sed  's/FA_base_o.vtp/$(word 2,$^)/g' $< > $@
	sed -i 's/FA_base.vtp/$(word 3,$^)/g' $@
	sed -i 's/FA_base_ell.vtp/$(word 4,$^)/g' $@

%.tsv : $$(BASENAME).ana $$(BASENAME).calc %_ell.vtpana %_FA-edges.csv %_FA.csv %.vtpana $$(BASENAME).neiN
	@echo "process $+ to $@"
	$(eval pos= $(subst $(call base_,$*),,$*))
	$(eval pos= $(subst _a,,$(pos)))
	echo -e "Q\tU\tV" > $@
	awk 'BEGIN{ln="$(pos)"; gsub ("^0*", "", ln)} {if($$1 == ln) printf("{$$V$$}\t{[mm$$^3$$]}\t%s\n{$$d_v$$}\t{[mm]}\t%s\n{$$S_c$$}\t{[mm$$^2$$]}\t%s\n", $$24, $$26, $$25)}' $< >> $@ # Vphy = $24, Aphy = $25, Dv = $26
	awk 'NR>1{printf("{$$S_o$$}\t{[mm$$^2$$]}\t%s\n", $$3)}' $(word 6,$^) >> $@ # So (phy): S = $3
	awk -v So=`awk 'NR>1{print $$3}'  $(word 6,$^)` 'BEGIN{ln="$(pos)"; gsub ("^0*", "", ln)} {if($$1 == ln) printf("{$$S_l/S_o$$}\t[]\t%f\n", $$25 / So - 1)}' $< >> $@ # Aphy = $25; So (phy): S = $3 in %.vtpana
	awk 'BEGIN{ln="$(pos)"; gsub ("^0*", "", ln)} {if($$1 == ln) printf("{$$\\Psi$$}\t[]\t%s\n", $$4)}' $(word 2,$^) >> $@ # Psi = $4
	awk -v Se=`awk '/all/{print $$3}'  $(word 3,$^)` 'BEGIN{ln="$(pos)"; gsub ("^0*", "", ln)} {if($$1 == ln) printf("{$$\\eta$$}\t[]\t%f\n", Se/$$25)}' $< >> $@ # Aphy = $25; Se (phy): S = $3 in %_ell.vtpana
	datamash -t, --header-in median 2 < $(word 4,$^) | xargs printf '{med($$\\theta_e$$)}\t{[$$^\\circ$$]}\t{%s}\n' >> $@ # {} for (possibly) empty cells https://tex.stackexchange.com/a/114768 # IPAe = 2, add e.g. mean 2 to also get mean
	awk -F, '$$7 ~ /^[[:digit:]]/{n++} END{printf("{$$\\Upsilon$$}\t[]\t%s\n", n)}' $(word 5,$^) >> $@ # FacetIds = 7
	awk 'BEGIN{ln="$(pos)"+0} {if($$2+0 == ln) printf("{$$\\Xi$$}\t[]\t%s\n", $$1)}' $(word 7,$^) >> $@ # label = $2, Xi = $1 # https://stackoverflow.com/questions/15515911/strip-leading-zeros-in-awk-program#15516151
