
SHELL:= /bin/bash


SUBDIRS:= SA_h3d

SUBDIRSCLEAN=$(addprefix clean,$(SUBDIRS))


.PHONY: all clean $(SUBDIRS)


all : $(SUBDIRS)

clean : $(SUBDIRSCLEAN)


.gitmodules : $(SUBDIRS:=/.gitmodules) # generate .gitmodules for subtrees containing submodules
	-rm $@ # expecting only subtrees and "subsubmodules", no direct submodules
	$(foreach O, $^, sed 's|submodules/|$(dir $O)submodules/|g' $O >> $@)

$(SUBDIRS) : | .gitmodules
	$(MAKE) -C $@

# .PHONY: $(SUBDIRSCLEAN) # does not work with PHONY, why?
clean% : # using prefix to cope with trailing / # http://stackoverflow.com/questions/26007005/using-makefile-to-clean-subdirectories#26007657
	$(MAKE) -C $* res-clean

